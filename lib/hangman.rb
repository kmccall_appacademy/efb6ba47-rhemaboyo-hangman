require 'byebug'
class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players = {})
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = []
  end

  def setup
    guesser.register_secret_length(referee.pick_secret_word)
    referee.pick_secret_word.times { board << nil }
  end

  def take_turn
    update_board(referee.check_guess(guesser.guess))
    guesser.handle_response
  end

  def update_board(places)
    places.each do |i|
      board[i] = guesser.guess
    end
  end

end

class HumanPlayer
end

class ComputerPlayer
  attr_accessor :dictionary, :candidate_words

  def initialize(dictionary)
    @dictionary = dictionary
    @candidate_words = dictionary
  end

  def pick_secret_word
    dictionary.sample.length
  end

  def check_guess(letter)
    places = []
    dictionary[0].chars.each_with_index do |ch, i|
      places << i if ch == letter
    end
    places
  end

  def register_secret_length(length)
    candidate_words.select! { |word| word.length == length }
  end

  def handle_response(letter, places)
    if places.empty?
      candidate_words.reject! { |word| word.include?(letter) }
    else
      candidate_words.select! do |word|
        places.all? { |i| word[i] == letter } &&
        word.count(letter) == places.count
      end
    end
  end

  def guess(board)
    letters = ('a'..'z').to_a.sort_by do |letter|
      candidate_words.join.count(letter)
    end
    if board.nil?
      letters.last
    else
      letters.reject! { |letter| board.include?(letter) }
      letters.last
    end
  end


end
